import time
from display import Display
from dataLoader import Getter
from nodered import Nodered

# Initialize library.
disp = Display();
dl = Getter();
nr = Nodered();



while True:
  toDisplay = [dl.getMem(), dl.getCpu(), dl.getUptime(), dl.getTemp()]
  consumed = dl.getConsumedInteret();
  icons = dl.getIcons();

  disp.printToDisplay(toDisplay)
  disp.printIcons(icons);
  disp.printInternet(consumed);


  nr.writeData(toDisplay)
  
  time.sleep(.5)
