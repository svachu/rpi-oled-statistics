import time

from board import SCL, SDA
import busio
import board
import digitalio
from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306

import subprocess

class Display:
  def __init__(self):
    # Create the I2C interface.
    self.i2c = busio.I2C(SCL, SDA)
 
    self.disp = adafruit_ssd1306.SSD1306_I2C(128, 64, self.i2c)
 
    # Clear display.
    self.disp.fill(0)
    self.disp.show()
 
    # Create blank image for drawing.
    # Make sure to create image with mode '1' for 1-bit color.
    self.width = self.disp.width
    self.height = self.disp.height
    self.image = Image.new("1", (self.width, self.height))
 
    # Get drawing object to draw on image.
    self.draw = ImageDraw.Draw(self.image)
 
    # Draw a black filled box to clear the image.
    self.draw.rectangle((0, 0, self.width, self.height), outline=0, fill=0)
 
    # Draw some shapes.
    #self.font = ImageFont.load_default()
    self.font = ImageFont.truetype('pixelmix.ttf', 8)
    self.iconFont = ImageFont.truetype('icon1.ttf', 10)
    self.iconFont2 = ImageFont.truetype('icon2.ttf', 10)
    self.iconFont3 = ImageFont.truetype('icon3.ttf', 20)
    self.fontWidth = 7;
  def printInternet(self, data):
    self.draw.rectangle((0,12,self.width,22), outline=0, fill=0)
    self.draw.text((0, 10)          ,  "A",            font=self.iconFont, fill=255)
    self.draw.text((self.fontWidth*1+1, 10)          ,  "B",            font=self.iconFont, fill=255)
    self.draw.text((self.fontWidth*2+2, 13), str(data["total"])+"MiB",  font=self.font, fill=255)
    self.draw.text((self.fontWidth*9, 10), "8",            font=self.iconFont, fill=255)
    self.draw.text((self.fontWidth*10+2, 13), str(data["consumed"])+"%",  font=self.font, fill=255)

    self.disp.image(self.image)
    self.disp.show()
  def printIcons(self, data):
    self.draw.rectangle((0,0,self.width,11), outline=0, fill=0)
    {"internet": True, "can": True, "nodered": True, "mqtt": True}
    text = "";
    if data['internet']:
        text += "s";
    else:
        text += " ";

    if data['can']:
        text += "p";
    else:
        text += " ";

    if data['nodered']:
        text += "l";
    else: 
        text += " ";

    if data['mqtt']:
        text += "k";
    else: 
        text += " ";

    self.draw.text((0, 0),     text,  font=self.iconFont, fill=255)

    self.disp.image(self.image)
    self.disp.show()

  def printToDisplay(self, lines):
    self.draw.rectangle((0,23,self.width,self.height), outline=0, fill=0)

    lineHeight = 9
    print("-------------------------")
    for i, line in enumerate(lines):
        self.draw.text((0, lineHeight*i + 23),     line,  font=self.font, fill=255)
        print(line.ljust(25) + "|")
    print("-------------------------")
    
    #self.draw.text((0, 32),     "ABC",  font=self.iconFont, fill=255)
    #self.draw.text((0, 42),     "ABC",  font=self.iconFont2, fill=255)
    # self.draw.text((64, 32),     "ABC",  font=self.iconFont3, fill=255)

    # Display image.
    self.disp.image(self.image)
    self.disp.show()
  




