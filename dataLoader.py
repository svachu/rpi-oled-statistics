import time
import json
import subprocess

class Getter:
  def getInternet(self):
    cmd = "ifconfig eth0 | awk '/TX packets/ {print $5}' | cut -d \":\" -f2"
    TX = subprocess.check_output(cmd, shell = True )
    cmd = "ifconfig ppp0 | awk '/RX packets/ {print $5}' | cut -d \":\" -f2"
    RX = subprocess.check_output(cmd, shell = True )

    internet = "No internet"
    if RX:
      TX = round(int(TX.decode().strip()) / 1024 / 1024, 1) 
      RX = round(int(RX.decode().strip()) / 1024 / 1024, 1) 
    
      internet = "RX: {} MiB / TX: {} MiB".format(TX, RX)
    
    return internet
  def getConsumedInteret(self):
    cmd = "vnstat -i ppp0 --json"
    consumption = subprocess.check_output(cmd, shell = True )
    con = json.loads(consumption)
    total = con["interfaces"][0]["traffic"]["total"]
    total["total"] = round((total["rx"] + total["tx"])/1024,1)
    total["consumed"] = round(((total["rx"] + total["tx"])/488281)*100, 2) # data plan is 500MB = 488281 KiB
    return total
  def getTemp(self):
    cmd = "vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*'"
    temp = subprocess.check_output(cmd, shell = True ).decode().strip()
    return "Temp: {} C".format(float(temp))
    
  def getMem(self):
    cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%sMB %.2f%%\", $3,$2,$3*100/$2 }'"
    MemUsage = subprocess.check_output(cmd, shell = True ).decode().strip()
    return MemUsage
    
  def getCpu(self):
    cmd = "uptime | tail -c 17"
    CPU = subprocess.check_output(cmd, shell = True ).decode().strip()
    return "Load: " + CPU
  
  def getUptime(self):
    cmd = "uptime | tr \",\" \" \" | cut -f4-5 -d\" \""
    CPU = subprocess.check_output(cmd, shell = True ).decode().strip()
    return "Uptime: " + CPU
  def getIcons(self):
    return {"internet": True, "can": True, "nodered": True, "mqtt": True}
   

dl = Getter()
print(dl.getConsumedInteret())
print(dl.getTemp())
print(dl.getMem())

